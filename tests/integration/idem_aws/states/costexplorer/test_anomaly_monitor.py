import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_anomaly_monitor(hub, ctx):
    # cost explorer is not supported in localstack and works in real aws
    if hub.tool.utils.is_running_localstack(ctx):
        return

    idem_monitor_name = "idem_monitor_name"
    monitor_name = "idem-test-monitor-" + str(uuid.uuid4())
    monitor_new_name = "idem-test-monitor-" + str(uuid.uuid4())
    dimensions = {"Key": "LINKED_ACCOUNT", "Values": ["820272282974"]}
    monitor_type = "CUSTOM"
    monitor_specification = {"Dimensions": dimensions}

    # Create anomaly monitor with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.costexplorer.anomaly_monitor.present(
        test_ctx,
        name=idem_monitor_name,
        monitor_name=monitor_name,
        monitor_specification=copy.deepcopy(monitor_specification),
        monitor_type=monitor_type,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create aws.costexplorer.anomaly_monitor {idem_monitor_name}"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert monitor_specification == resource.get("anomaly_monitor").get(
        "monitor_specification"
    )
    assert monitor_type == resource.get("anomaly_monitor").get("monitor_type")
    assert monitor_name == resource.get("anomaly_monitor").get("monitor_name")

    # create monitor in real
    ret = await hub.states.aws.costexplorer.anomaly_monitor.present(
        ctx,
        name=idem_monitor_name,
        monitor_name=monitor_name,
        monitor_specification=copy.deepcopy(monitor_specification),
        monitor_type=monitor_type,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert (
        f"Created aws.costexplorer.anomaly_monitor '{idem_monitor_name}'"
        in ret["comment"]
    )
    resource = ret.get("new_state")
    assert monitor_specification == resource.get("monitor_specification")
    assert monitor_type == resource.get("monitor_type")
    assert monitor_name == resource.get("monitor_name")
    resource_id = resource.get("resource_id")

    # Describe anomaly_monitor
    describe_ret = await hub.states.aws.costexplorer.anomaly_monitor.describe(ctx)
    assert describe_ret

    # Verify that describe output format is correct
    assert "aws.costexplorer.anomaly_monitor.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "aws.costexplorer.anomaly_monitor.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert monitor_type == described_resource_map["monitor_type"]
    assert monitor_specification == described_resource_map["monitor_specification"]

    # Update monitor with test flag
    ret = await hub.states.aws.costexplorer.anomaly_monitor.present(
        test_ctx,
        name=idem_monitor_name,
        monitor_name=monitor_new_name,
        resource_id=resource_id,
        monitor_type=monitor_type,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert monitor_new_name == resource.get("monitor_name")
    assert monitor_type == resource.get("monitor_type")

    # update monitor in real
    ret = await hub.states.aws.costexplorer.anomaly_monitor.present(
        ctx,
        name=idem_monitor_name,
        monitor_name=monitor_new_name,
        resource_id=resource_id,
        monitor_type=monitor_type,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Update monitor_name: {monitor_new_name}" in ret["comment"]
    resource = ret.get("new_state")
    assert monitor_new_name == resource.get("monitor_name")
    assert monitor_type == resource.get("monitor_type")

    # already present scenario
    ret = await hub.states.aws.costexplorer.anomaly_monitor.present(
        ctx,
        name=idem_monitor_name,
        monitor_name=monitor_new_name,
        resource_id=resource_id,
        monitor_type=monitor_type,
    )
    assert ret["result"], ret["comment"]
    assert f"{idem_monitor_name} already exists" in ret["comment"]
    assert hub.tool.aws.state_comparison_utils.compare_dicts(
        ret.get("old_state"),
        ret.get("new_state"),
    )
    resource = ret.get("new_state")
    assert monitor_new_name == resource.get("monitor_name")
    assert monitor_type == resource.get("monitor_type")

    # Delete monitor with test flag
    ret = await hub.states.aws.costexplorer.anomaly_monitor.absent(
        test_ctx,
        name=idem_monitor_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete aws.costexplorer.anomaly_monitor {idem_monitor_name}"
        in ret["comment"]
    )
    resource = ret.get("old_state")
    assert monitor_new_name == resource.get("monitor_name")
    assert monitor_type == resource.get("monitor_type")

    # Delete monitor in real
    ret = await hub.states.aws.costexplorer.anomaly_monitor.absent(
        ctx,
        name=idem_monitor_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Deleted aws.costexplorer.anomaly_monitor '{idem_monitor_name}'"
        in ret["comment"]
    )
    resource = ret.get("old_state")
    assert monitor_new_name == resource.get("monitor_name")
    assert monitor_type == resource.get("monitor_type")

    # Deleting monitor again should be a no-op
    ret = await hub.states.aws.costexplorer.anomaly_monitor.absent(
        ctx,
        name=idem_monitor_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        f"aws.costexplorer.anomaly_monitor '{idem_monitor_name}' already absent"
        in ret["comment"]
    )
