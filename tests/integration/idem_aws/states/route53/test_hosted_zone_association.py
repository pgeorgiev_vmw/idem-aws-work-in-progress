import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_hosted_zone_association(
    hub, ctx, aws_ec2_vpc_2, aws_route53_private_hosted_zone
):
    # localstack is not supporting creation of private hosted zones. It gives error 500 Internal server error.
    # We can only attach vpc to private hosted zones. so this test can be only run on real AWS.
    if hub.tool.utils.is_running_localstack(ctx):
        return
    hosted_zone_vpc_association_temp_name = (
        "idem-test-hosted_zone-vpc-association" + str(uuid.uuid4())
    )
    zone_id = aws_route53_private_hosted_zone.get("resource_id")
    vpc_id = aws_ec2_vpc_2.get("VpcId")

    # create zone association with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    vpc_region = ctx["acct"].get("region_name")

    # create zone association with test flag
    ret = await hub.states.aws.route53.hosted_zone_association.present(
        test_ctx,
        name=hosted_zone_vpc_association_temp_name,
        zone_id=zone_id,
        vpc_id=vpc_id,
        vpc_region=vpc_region,
        comment="test comment",
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type="aws.route53.hosted_zone_association",
            name=hosted_zone_vpc_association_temp_name,
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert zone_id == resource.get("zone_id")
    assert vpc_id == resource.get("vpc_id")
    assert vpc_region == resource.get("vpc_region")
    assert ret["new_state"].get("resource_id")
    assert "test comment" == ret["new_state"]["comment"]

    # create zone association
    ret = await hub.states.aws.route53.hosted_zone_association.present(
        ctx,
        name=hosted_zone_vpc_association_temp_name,
        zone_id=zone_id,
        vpc_id=vpc_id,
        vpc_region=vpc_region,
        comment="test comment",
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource_id = f"{zone_id}:{vpc_id}:{vpc_region}"
    assert resource_id == ret["new_state"]["resource_id"]
    resource = ret.get("new_state")
    assert zone_id == resource.get("zone_id")
    assert vpc_id == resource.get("vpc_id")
    assert vpc_region == resource.get("vpc_region")
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.route53.hosted_zone_association",
            name=hosted_zone_vpc_association_temp_name,
        )[0]
        in ret["comment"]
    )

    describe_ret = await hub.states.aws.route53.hosted_zone_association.describe(ctx)
    assert resource_id in describe_ret
    assert "aws.route53.hosted_zone_association.present" in describe_ret.get(
        resource_id
    )
    described_resource = describe_ret.get(resource_id).get(
        "aws.route53.hosted_zone_association.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "resource_id" in described_resource_map
    assert f"{zone_id}:{vpc_id}:{vpc_region}" == described_resource_map.get(
        "resource_id"
    )
    assert "vpc_id" in described_resource_map
    assert vpc_id == described_resource_map.get("vpc_id")
    assert "vpc_region" in described_resource_map
    assert vpc_region == described_resource_map.get("vpc_region")
    assert "zone_id" in described_resource_map
    assert zone_id == described_resource_map.get("zone_id")

    resource_id = described_resource_map.get("resource_id")
    ret = await hub.states.aws.route53.hosted_zone_association.present(
        ctx,
        name=hosted_zone_vpc_association_temp_name,
        resource_id=resource_id,
        zone_id=zone_id,
        vpc_id=vpc_id,
        vpc_region=vpc_region,
        comment="test comment",
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"aws.route53.hosted_zone_association '{hosted_zone_vpc_association_temp_name}' association already exists"
        in ret["comment"]
    )
    resource = ret.get("old_state")
    assert zone_id == resource.get("zone_id")
    assert vpc_id == resource.get("vpc_id")
    assert vpc_region == resource.get("vpc_region")

    resource = ret.get("new_state")
    assert zone_id == resource.get("zone_id")
    assert vpc_id == resource.get("vpc_id")
    assert vpc_region == resource.get("vpc_region")

    # delete zone association with test flag
    ret = await hub.states.aws.route53.hosted_zone_association.absent(
        test_ctx,
        name=hosted_zone_vpc_association_temp_name,
        resource_id=resource_id,
    )
    assert ret["result"] and ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.route53.hosted_zone_association",
            name=hosted_zone_vpc_association_temp_name,
        )[0]
        in ret["comment"]
    )
    resource = ret.get("old_state")
    assert zone_id == resource.get("zone_id")
    assert vpc_id == resource.get("vpc_id")
    assert vpc_region == resource.get("vpc_region")

    ret = await hub.states.aws.route53.hosted_zone_association.absent(
        ctx,
        name=hosted_zone_vpc_association_temp_name,
        resource_id=resource_id,
    )

    assert ret["result"] and ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.route53.hosted_zone_association",
            name=hosted_zone_vpc_association_temp_name,
        )[0]
        in ret["comment"]
    )
    resource = ret.get("old_state")
    assert zone_id == resource.get("zone_id")
    assert vpc_id == resource.get("vpc_id")
    assert vpc_region == resource.get("vpc_region")

    ret = await hub.states.aws.route53.hosted_zone_association.absent(
        ctx,
        name=hosted_zone_vpc_association_temp_name,
        resource_id=resource_id,
    )
    assert ret["result"] and ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.route53.hosted_zone_association",
            name=hosted_zone_vpc_association_temp_name,
        )[0]
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_hosted_zone_association_absent_with_none_resource_id(hub, ctx):
    name = "idem-test-hosted_zone-vpc-association" + str(uuid.uuid4())
    # Delete hosted zone association with resource_id as None. Result in no-op.
    ret = await hub.states.aws.route53.hosted_zone_association.absent(
        ctx,
        name=name,
        resource_id=None,
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.route53.hosted_zone_association", name=name
        )[0]
        in ret["comment"]
    )
